Cgo, Runtime & Misc
24.02.2017

fmi@golang.bg
http://fmi.golang.bg/

* В тази лекция:

- Една истинска трагедия
- Cgo
- unsafe
- Go in Go history
- GODEBUG
- runtime stuff

* Излъгах, че сме взели целия език

- Тайната на успеха на Go!
- Супер важно, пет нови ключови думи!!
- `notwithstanding`, `thetruthofthematter`, `despiteallobjections`, `whereas`, `insofaras`
- Абсолютно валидни и запазени ключови думи... [[https://github.com/golang/go/blob/release-branch.go1.7/src/cmd/compile/internal/gc/lex.go#L610][които не правят нищо]]

.play code/goruntime/easter_eggs.go

- Махнали са ги в [[https://github.com/golang/go/blob/master/src/cmd/compile/internal/syntax/tokens.go#L138][1.8]]... :)

* C? Go? Cgo!

* C в .go файлове (1)

- Споменавали сме ви, че можете да използвате C библиотеки в go
- Бърз пример как се прави:

.code code/cgo-runtime/cgo.go

- Няма пакет "C"

* C в .go файлове (2)

Накратко:

- import-ваме несъществуващия пакет "C"
- в коментари преди това пишем C код и препроцесорни директиви
- от Go кода извикваме функциите с `C.nesho_si()`
- стискаме палци и зъби

* Особености

- Коментарите трябва да се точно преди импорта на "C"
- Те стават header на компилирания С сорс
- Коментари, започващи с `#cgo` са "специални"

    // #cgo CFLAGS: -DPNG_DEBUG=1
    // #cgo amd64 386 CFLAGS: -DX86=1
    // #cgo LDFLAGS: -lpng
    // #include <png.h>
    import "C"

- CPPFLAGS and LDFLAGS  могат да бъдат "наследявани" от други пакети:

    // #cgo pkg-config: png cairo


* Go в .c файлове (1)

- А вече може да използвате Go библиотеки в C

.code code/cgo-runtime/goc/goc.go

- Важното: //export <function-name>

* Go в .c файлове (2)

    go build -buildmode=c-archive -o goc.a goc.go

- Ще се генерират `goc.a` и `goc.h` файлове
- В `goc.h`, освен малко boilerplate, ще има и:

    extern void GreetFromGo(GoString p0);

- `GoString` е част от споменатия boilerplate

* Go в .c файлове (3)

- Сега можем да го използваме

.code code/cgo-runtime/goc/goc.c

- След това събираме всичко със

    gcc -o out goc.c goc.a

* Никога не е толкова просто
.link https://github.com/golang/go/wiki/cgo
.link https://golang.org/src/cmd/cgo/doc.go

Стигне ли се до компилиране на C, забравете за лесно:

- Статично линкване
- Cross компилиране

Но, за това пък, има много от:

- Wrapper-и във wrapper-и
- Ръчно управление на памет
- Увещаване на linker-а да ви събере всичките частички

* Ситният шрифт (2)

Споделяне на памет, алокирана от Go е възможно ако:

- споделената памет не съдържа указатели към друга алокирана от Go памет
- C не задържи указателя към паметта, след като върне

runtime проверява за това и ако види нарушение crash-ва програмата.


* Още от магиите на "C"

- В .go файловете, полетата на структури са достъпни с _. Ако в структурата x има поле foo, то е достъпно с x._foo
- Стандартните типове са достъпни през C, с леки промени. `unsigned`int` -> `C.uint`
- Директният достъп до struct, union или enum също е особен: `C.struct_foo`
- В този ред на мисли go не съпортва union, а се представят като масиви от байтове
- И докато сме на темата с ограниченията, не можете да вкарвате поле със C тип в go struct


* Error handling

- Няма нужда да се отказвате от адекватния error handling
- Всяка C функция може да се извиква с две стойности от ляво. Втората очевидно е `errno`


    n, err := C.sqrt(-1)


* Function pointers

- Лошата новина: не може да викате функция по указател
- Добрата новина: можете да я пазите в променлива

* Мoля?

.code code/cgo-runtime/func_point.go


* Друг начин за викане на Go от C

    package main

    func Add(a, b int) int {
        return a + b
    }


* Друг начин за викане на Go от C (2)

    #include <stdio.h>

    extern int go_add(int, int) __asm__ ("example.main.Add");

    int main() {
      int x = go_add(2, 3);
      printf("Result: %d\n", x);
    }


* Друг начин за викане на Go от C (3)

- Само за `gccgo`
- Спомняте ли си, че има и друг компилатор?

    all: main

    main: foo.o bar.c
        gcc foo.o bar.c -o main

    foo.o: foo.go
        gccgo -c foo.go -o foo.o -fgo-prefix=example

    clean:
        rm -f main *.o


* Указатели без граници

- Ако си играете със C рано или късно ще попаднете на void*
- Навярно ще ви се наложи и да освободите паметта сочена от указател, който ви е бил върнат
- Може даже да ви се наложи да заделите памет и да подадете указател (като void*) на някоя C функция
- void* на Go-шки е unsafe.Pointer


* package unsafe

Декларира _невинно_ изглеждащите:

- `func`Alignof(v`ArbitraryType)`uintptr`
- `func`Offsetof(v`ArbitraryType)`uintptr`
- `func`Sizeof(v`ArbitraryType)`uintptr`
- `type`ArbitraryType`int`
- `type`Pointer`*ArbitraryType`

реално тези дефиниции съществуват главно за документация, имплементацията е в компилатора.

* Safety third

unsafe.Pointer има четири важни харектеристики

- указател към какъв да е тип може да бъде конвертиран към unsafe.Pointer
- unsafe.Pointer може да бъде конвертиран към указател към какъв да е тип
- uintpr може да бъде конвертиран към unsafe.Pointer
- unsafe.Pointer може да бъде конвертиран към uintptr

Това е практическо заобикаляне на типовата система в Go.

* Unsafe пример:

.play code/cgo-runtime/unsafe.go


* Адресна аритметика:

.play code/cgo-runtime/unsafe_2.go /struct/,


* Go in Go

* С? Защо?

- Вече готови инструменти в Plan9
- Познавайки тези иструменти, могат да се движат бързо

* Компилатор

- Вече изцяло на Go, всичкото С го няма
- Не го правят за да се тупат в гърдите
- Коректно Go се пише по - лесно от коректно С
- ... и се дебъгва по - лесно, дори без дебъгер
- Go прави параленото изпълнение тривиално

* Runtime

- До 1.5 са имали нужда от специален компилатор за runtime-а на езика
- Вече го няма. Един компилтор по - малко.
- Само един език. По - лесна интеграция, управление на стека
- Простота

* Как се махат хиляди редове С?

- Транслатор от С до (лошо!) Go
- Специално написан за тази задача, далеч от генерален
- Махане на странни С идиоми (*p++)
- Statement by statement принтиране на Go код от С
- Compile, Run, Compare, Repeat
.link https://www.youtube.com/watch?v=cF1zJYkBW4A

* Производителност

- 1.5 е първа стъпка
- Компилирането на Go програми е ~2 пъти по - бавно
- Но пък, 2 * много бързо = просто бързо
- От друга страна: go profiling(!) и други подобрения
- Прави много по - лесни оптимизации в бъдеще

* Също преминали в Go Land:

- Assembler
- Linker

* GODEBUG

* Добре, компилира се!

- Вече програмата работи
- ... почти
- ... прави неща, които не разбирам
- Какво!? Сигурен съм, че имах повече от 100 MB свободна рам!

* Повече информация

- Можете да получите повече информация за работеща програма с флагове на GODEBUG променливата на средата:

    export GODEBUG="name=flag"

Пример:

    export GODEBUG="gctrace=2,invalidptr=1"

Позволява:

- Проследяваме на всяка алокация и освобаждаване на памет (allocfreetrace)
- Спрете конкурентния GC. Всичко става stop-the-world (gcstoptheworld)
- Информация за всяко пускане на GC (gctrace)

* ...

- Показване на състоянието на scheduler-a (scheddetail и schedtrace)
- Спрете намаляването на goroutine стековете (gcshrinkstackoff)
- Всяка алокация да бъде на нова страница и да не се преизползват адреси (efence)
- Много други

* GOGC

- Променлива на средата, подобна на GODEBUG
- Определя колко често да се пуска garbage collector-а
- Стойноста е колко процента трябва да стане ново алокираното пространство в heap-a спрямо "живия" heap. След стигането на тази стойност се пуска чистенето на боклук.
- По подразбиране `GOGC=100`
- Възможни са всякакви числа, 200, 300
- `GOGC=off` спира събирането на боклук изцяло
- Има и функция `SetGCPercent()` в `runtime/debug` пакета, с която може да се променя по време на изпълнение

* GOMAXPROCS

- Променлива на средата
- Функция `GOMAXPROCS(n`int)` от пакета `runtime`
- Определя на максимално колко истински нишки от операционната система ще се изпълнява *вашия* Go код
