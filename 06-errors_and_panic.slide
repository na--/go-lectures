Обработване на грешки
17.02.2017

fmi@golang.bg
http://fmi.golang.bg/

* Error handling


* Имало едно време чисто С

- Неконсистентен `error`handling`
- Понякога се приема аргумент по указател, в който се записва евентуална грешка
- Понякога се ползва връщаната стойност
- Понякога това е комбинирано с `errno`

* Пример в C

.code code/errors_and_testing/c_err_example.c

* Имало едно време един език Go

Има грубо-казано 2 начина

- 1) Връщане на грешка като (част от) резултата от функция
- 2) Изпадане в паника (носете си кърпа)


* Връщане на грешка

- Има конвенция обектите, които се връщат, да отговарят на следния глобално-достъпен интерфейс:

    type error interface {
        Error() string
    }

- Разбира се, всеки може да връща "по-сложни" обекти, даващи повече информация за грешката. Например, `os.Open` връща `os.PathError`:

    type PathError struct {
        Op string    // "open", "unlink", etc.
        Path string  // Файлът с грешката
        Err error    // Грешката, върната от system call-a
    }

    func (e *PathError) Error() string {
        return e.Op + " " + e.Path + ": " + e.Err.Error()
    }

* Стандартна употреба

    func ReadFile(filename string) ([]byte, error) {
        f, err := os.Open(filename)
        if err != nil {
            return nil, err
        }
        //...
    }

или малко по-сложно:

    func CreateFile(filename string) (*os.File, error) {
        var file, err = os.Create(filename)
        if e, ok := err.(*os.PathError); ok && e.Err == syscall.ENOSPC {
            deleteTempFiles() // Free some space
            return os.Create(filename)
        }
        return file, err
    }

* Errors are values

Често оплакване на Go програмисти е количеството проверки за грешки:

    if err != nil {
        return err
    }

- Това е донякъде вярно, особено ако даден код се обръща често към "външния свят" (`os`, `io`, `net`, etc.)
- За разлика от други езици, които може да имат exceptions и try-catch, в Go грешките се третират като нормални стойности
- Това е умишлено, защото помага за обработката на всички грешки на правилното място
- Така нямаме глобални "try-catch" блокове и изненадващи exceptions, които идват от 20 нива навътре в call stack-а
- Повече подробности на [[https://blog.golang.org/errors-are-values]]

* Пример

    if _, err := fd.Write(p0[a:b]); err != nil {
        return err
    }
    if _, err := fd.Write(p1[c:d]); err != nil {
        return err
    }
    if _, err := fd.Write(p2[e:f]); err != nil {
        return err
    }

Може да стане:

    var err error
    write := func(buf []byte) {
        if err == nil {
            _, err = w.Write(buf)
        }
    }
    write(p0[a:b])
    write(p1[c:d])
    write(p2[e:f])
    if err != nil {
        return err
    }

* Създаване на грешки

- може да връщате собствен тип, който имплементира `error` интерфейса
- може да използвате функцията `New(string)` от пакета `errors`:

    func someFunc(a int) (someResult, error) {
        if a <= 0 {
            return nil, errors.New("a must be positive!")
        }
        // ...
    }

- може да използвате `fmt.Errorf`:

    func someFunc(a int) (someResult, error) {
        if a <= 0 {
            return nil, fmt.Errorf("a is %d, but it must be positive!", a)
        }
        // ...
    }

- може да си направите и собствен "конструктор"

* Припомняне на defer

- `defer` е специален механизъм на езика
- `defer` добавя *извикване* на функция в един списък (стек)
- Когато обграждащата функция приключи, тези извиквания се изпълняват в обратен ред

.play code/errors_and_testing/defer_example.go /^func main/,/^}/

- defer се използва за сигурно и лесно почистване на ресурси (отворени файлове, заключени mutex-и, etc.) ... и справяне с панирани програми!

* Паника!

- Нещо като изключенията
- Ползваме ги само в крайни случаи (не като изключенията)
- Изпадайки в паника, подавате стринг с грешката
- Добрата новина е, че можете да се съвземате от тях... пак буквално


* Уточнения

- `panic` е вградена функция
- Тя спира нормалното изпълнение на програмата
- Когато функция F изпълни `panic`, изпълнението на F спира, всички `defer`-нати функции на F се изпълняват нормално, след което изпълнението се връща във функцията, извикала F
- За извикващия на F, F е все едно извикване на `panic`
- Това продължава, докато всички функции в текущата горутина (`thread`) не свършат, когато програмата гърми
- Паники се случват след директното извикване на функцията `panic`, както и след разни runtime грешки, като `out-of-bounds`array`access`

* Избягвайте ненужното изпадане в паника
.image assets/panic.jpg 550 500

* recover

- Съвзема от паника
- `recover` е безполезен без `defer` ( може да се съвземете само в defer )
- `recover` не прави нищо (връща `nil`), ако текущата горутина не е в паника
- Ако е, `recover` връща аргумента, подаден на `panic`

* Example

.play code/errors_and_testing/panic.go /^func f/,

* Go 1.8

[[https://blog.golang.org/go1.8]]

[[https://golang.org/doc/go1.8]]

* Sneak peek: Тестове

- В Go има вградени инструменти и пакет от стандартната библиотека за различни видове автоматизирани тестове
- За да тестваме `foo.go`, създаваме `foo_test.go` в същата директория, който тества `foo.go`
- С `go`test`./...` пускаме тестовете на един цял проект
- Повече подробности... другата седмица :)


* Домашно
